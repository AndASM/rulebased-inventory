-----------------------------------------------------------------------------------
-- Addon Name: Rulebased Inventory
-- Creator: TaxTalis
-- Addon Ideal: user-defined rule-based actions for inventory application
-- Addon Creation Date: September 11, 2018
--
-- File Name: Event.lua
-- File Description: This file contains the event listener definition
-- Load Order Requirements: after Task
--
-----------------------------------------------------------------------------------

-- imports
local RbI = RulebasedInventory
local ruleEnv = RbI.ruleEnvironment
local ctx = RbI.executionEnv.ctx
local utils = RbI.utils

RbI.ItemQueue = utils.NewAsyncQueue(true, 1,
	function(itemLink) -- nextFn
		RbI.CheckItemInBackpack(itemLink)
	end,
	function(a, b) -- equalsFn
		return a == b
	end
)

local function OnSlotUpdate(eventCode, bag, slot, isNewItem, itemSoundCategory, inventoryUpdateReason, stackCountChange)
	if((bag == BAG_BACKPACK or bag == BAG_VIRTUAL)
		and SCENE_MANAGER:GetCurrentScene() ~= STABLES_SCENE
		and not (Roomba and Roomba.WorkInProgress and Roomba.WorkInProgress())) then
		--d(eventCode .. ' ' .. bag .. ' ' .. slot .. ' '..inventoryUpdateReason..' '..tostring(itemSoundCategory)..' '..stackCountChange)

		if(isNewItem) then
			RbI.tasks.internal['Notify'].CheckBySlot(slot, bag, stackCountChange)
		end
		
		if(bag == BAG_BACKPACK) then
			local slotStackSize = GetSlotStackSize(bag, slot)
			--d("SlotStackSize: "..tostring(slotStackSize))
			if(slotStackSize > 0) then
				local itemLink = GetItemLink(bag, slot)
				--local _, stackItemToUse = GetItemInfo(bag, slot)
				--d("Push item: "..itemLink..": "..stackItemToUse.." ("..stackCountChange..")")
				RbI.ItemQueue.Push(itemLink) -- use, junk, destroy, fcoisMark
			end
		end
	end
end

local function DelayedCall(func)
	zo_callLater(func, RbI.account.settings.taskDelay)
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- BANK ----------------------------------------------------------------------
------------------------------------------------------------------------------

local function OnBankOpen(eventCode, bagId)
	local Task1, Task2, Task1ItemLink, Task1BagFrom, Task1bagTo, Task2ItemLink, Task2BagFrom, Task2bagTo
	if(bagId == BAG_BANK) then
		--d('Bank Opened')

		DelayedCall(
			function() RbI.TaskStarter(RbI.tasks.internal['CraftToBag']) end
		)
		
		RbI.allowAction['BagToBank'] = true
		RbI.allowAction['BankToBag'] = true
		Task1 = RbI.tasks.internal['BankToBag']
		Task2 = RbI.tasks.internal['BagToBank']
		
	end
	-- Possible spot for other "banks"
	
	if(Task1 ~= nil) then
		DelayedCall(
			function() RbI.TaskStarter(Task1, Task1ItemLink, Task1BagFrom, Task1bagTo, Task2, Task2ItemLink, Task2BagFrom, Task2bagTo) end
		)
	end
end

local function OnBankClose()
	--d('Bank Closed')
	RbI.allowAction['BagToBank'] = false
	RbI.allowAction['BankToBag'] = false
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- STORE ---------------------------------------------------------------------
------------------------------------------------------------------------------

local function OnStoreOpen()
	--d('Shop Opened')
	RbI.allowAction['Sell'] = true
	DelayedCall(
		function() RbI.TaskStarter(RbI.tasks.internal['Sell']) end
	)
end

local function OnStoreClose()
	--d('Shop Closed')
	RbI.allowAction['Sell'] = false
end


------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- FENCE ---------------------------------------------------------------------
------------------------------------------------------------------------------

local function OnFenceOpen()
	--d('Fence Opened')
	RbI.allowAction['Fence'] = true
	RbI.allowAction['Launder'] = true
	
	RbI.fenceTotal, RbI.fenceUsed = GetFenceSellTransactionInfo()
	RbI.launderTotal, RbI.launderUsed = GetFenceLaunderTransactionInfo()
	
	local Task1, Task2, Task1ItemLink, Task1BagFrom, Task1bagTo, Task2ItemLink, Task2BagFrom, Task2bagTo
	Task1 = RbI.tasks.internal['Launder']
	Task2 = RbI.tasks.internal['Fence']
	DelayedCall(
		function() RbI.TaskStarter(Task1, Task1ItemLink, Task1BagFrom, Task1bagTo, Task2, Task2ItemLink, Task2BagFrom, Task2bagTo) end
	)
end

local function OnFenceClose()
	--d('Fence Closed')
	RbI.allowAction['Fence'] = false
	RbI.allowAction['Launder'] = false
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- CRAFTING ------------------------------------------------------------------
------------------------------------------------------------------------------

local function WaitForPanel(topLevelPanelName)
	if(RbI.allowAction['Deconstruct'] or RbI.allowAction['Refine'] or RbI.allowAction['Extract']) then
		local TopLevelPanel = nil
		local Task = nil
		if(topLevelPanelName == 'DECONSTRUCTION') then
			TopLevelPanel = ZO_SmithingTopLevelDeconstructionPanelSlotContainer
			Task = RbI.tasks.internal['Deconstruct']
		elseif(topLevelPanelName == 'EXTRACTION') then
			TopLevelPanel = ZO_EnchantingTopLevelExtractionSlotContainer
			Task = RbI.tasks.internal['Extract']
		elseif(topLevelPanelName == 'REFINEMENT') then
			TopLevelPanel = ZO_SmithingTopLevelRefinementPanelSlotContainer
			Task = RbI.tasks.internal['Refine']
		end
		
		if(TopLevelPanel:IsHidden()) then
			zo_callLater(function() WaitForPanel(topLevelPanelName) end, 300)
		else
			DelayedCall(
				function() RbI.TaskStarter(Task, nil, topLevelPanelName) end
			)
		end
	end
end

local function OnCraftingStationOpen(eventCode, craftingType, sameStation, craftingMode)
	if(craftingType ~= CRAFTING_TYPE_ALCHEMY and craftingType ~= CRAFTING_TYPE_PROVISIONING) then
		-- d('RbI: Station Opened: ', craftingType, sameStation, craftingMode )
		RbI.allowAction['Deconstruct'] = true
		RbI.allowAction['Refine'] = true
		RbI.allowAction['Extract'] = true

		ctx.station = craftingMode ~= CRAFTING_INTERACTION_MODE_UNIVERSAL_DECONSTRUCTION and craftingType or nil
    -- d(ctx)

		if(RbI.account.settings.immediateExecutionAtCraftStation) then
			if (craftingMode == CRAFTING_INTERACTION_MODE_UNIVERSAL_DECONSTRUCTION) then
				DelayedCall(
					function()
						RbI.TaskStarter(RbI.tasks.internal['Deconstruct'], nil, 'DECONSTRUCTION', nil, RbI.tasks.internal['Extract'], nil, 'EXTRACTION', nil)
					end
				)
			elseif(craftingType ~= CRAFTING_TYPE_ENCHANTING) then
				DelayedCall(
					function()
						RbI.TaskStarter(RbI.tasks.internal['Deconstruct'], nil, 'DECONSTRUCTION', nil, RbI.tasks.internal['Refine'], nil, 'REFINEMENT')
					end
				)
			else
				DelayedCall(
					function()
						RbI.TaskStarter(RbI.tasks.internal['Extract'], nil, 'EXTRACTION')
					end
				)
			end
		else
			if(craftingType ~= CRAFTING_TYPE_ENCHANTING) then
				WaitForPanel('REFINEMENT')
				WaitForPanel('DECONSTRUCTION')
			else
				WaitForPanel('EXTRACTION')
			end
		end
	end
end

local function OnCraftingStationClose()
	RbI.allowAction['Deconstruct'] = false
	RbI.allowAction['Refine'] = false
	RbI.allowAction['Extract'] = false
	ctx.station = nil
	--d('Station Closed')
end


------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- CraftBag ------------------------------------------------------------------
------------------------------------------------------------------------------

-- only fires if items where moved from the inventory to the craft bag after login or fast traveling into another zone
-- it doesn't fire when an item goes directly into the craft bag (p.e: through looting)
EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_INVENTORY_ITEMS_AUTO_TRANSFERRED_TO_CRAFT_BAG, function()
  DelayedCall(
    function() RbI.TaskStarter(RbI.tasks.internal['CraftToBag']) end
  )
end)

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Initial PlayerActivated ---------------------------------------------------
------------------------------------------------------------------------------

function RbI.ConsistencyCheck()
	if (FCOIS and not FCOIS.addonVars.gPlayerActivated) then -- wait for it
		zo_callLater(RbI.ConsistencyCheck, 300)
	else
		RbI.TaskStarterCheckAllItems(BAG_BACKPACK, true)
	end
end

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Loot ----------------------------------------------------------------------
------------------------------------------------------------------------------

local function checkSafeLoot(numLootTransmute)
	local max = GetMaxPossibleCurrency(CURT_CHAOTIC_CREATIA, CURRENCY_LOCATION_ACCOUNT)
	local numTransmute = GetCurrencyAmount(CURT_CHAOTIC_CREATIA, CURRENCY_LOCATION_ACCOUNT)
	local sum = numTransmute + numLootTransmute
	if(sum > max) then
		return "Transmutations crystals would exceed the maximum, so we won't loot it: "..sum.." > "..max
	end
end

local currencies = {CURT_CHAOTIC_CREATIA}
local function lootAll(fnOnLootId)
	local content = {}
	if(fnOnLootId == nil) then
		LootAll()
		-- also loot currencies
		for _, currency in pairs(currencies) do
			local currencyLoot = GetLootCurrency(currency)
			if(currencyLoot > 0) then
				local name = GetCurrencyName(currency)
				content[#content+1] = currencyLoot..'x '..name
			end
		end
	end
	for i = 1, GetNumLootItems() do
		local lootId, name,_,count,_,_,_,_,lootType = GetLootItemInfo(i)
		local itemLink = GetLootItemLink(lootId)
		if(fnOnLootId ~= nil) then fnOnLootId(lootId) end
		if(count > 0) then
			content[#content+1] = count..'x '..itemLink
		else
			content[#content+1] = itemLink
		end
	end
	RbI.useLoot = table.concat(content, ", ")
end

-- loot only items without currency
local function lootOnlyItems()
	lootAll(LootItemById)
	EndLooting()
end

local function safeLoot()
	local numLootTransmute = GetLootCurrency(CURT_CHAOTIC_CREATIA)
	if numLootTransmute == 0 then
		lootAll()
	else
		local lootError = checkSafeLoot(numLootTransmute)
		if(type(lootError) ~= 'string') then
			lootAll()
		else
			-- loot only items no currency
			-- LootCurrency(slot.lootEntry.currencyType)
			lootOnlyItems()
			return lootError
		end
	end
end

function RbI.isLooting()
	return (GetGameTimeMilliseconds() - (RbI.useItemTime or 0)) < 1000
end

function RbI.RegisterForEvents()
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_INVENTORY_SINGLE_SLOT_UPDATE, OnSlotUpdate)
	-- Filter Events
	EVENT_MANAGER:AddFilterForEvent(RbI.name, EVENT_INVENTORY_SINGLE_SLOT_UPDATE, REGISTER_FILTER_INVENTORY_UPDATE_REASON, INVENTORY_UPDATE_REASON_DEFAULT)
	EVENT_MANAGER:AddFilterForEvent(RbI.name, EVENT_INVENTORY_SINGLE_SLOT_UPDATE, REGISTER_FILTER_UNIT_TAG, "player")

	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_OPEN_BANK, OnBankOpen)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_CLOSE_BANK, OnBankClose)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_OPEN_STORE, OnStoreOpen)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_CLOSE_STORE, OnStoreClose)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_OPEN_FENCE, OnFenceOpen)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_CLOSE_FENCE, OnFenceClose)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_CRAFTING_STATION_INTERACT, OnCraftingStationOpen)
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_END_CRAFTING_STATION_INTERACT, OnCraftingStationClose)

	-- put all items in the item queue
	EVENT_MANAGER:RegisterForEvent(RbI.name, EVENT_PLAYER_ACTIVATED, function()
		EVENT_MANAGER:UnregisterForEvent(RbI.name, EVENT_PLAYER_ACTIVATED)
		if (RbI.account.settings.startupitemcheck) then RbI.ConsistencyCheck() end
	end)

	ZO_PreHook(SYSTEMS:GetObject("loot"), "UpdateLootWindow", function(...)
		local name, targetType, actionName, isOwned = GetLootTargetInfo()
		-- d("name: ", name, targetType, actionName, isOwned)
		-- INTERACT_TARGET_TYPE_OBJECT corpses only appear, when eso auto-looting is off
		if (targetType == INTERACT_TARGET_TYPE_ITEM and RbI.isLooting()) then
			local lootError = safeLoot(...)
			if (type(lootError) == 'string') then
				RbI.useLootRevoke = lootError
			end
			return true
		end
	end)
end
